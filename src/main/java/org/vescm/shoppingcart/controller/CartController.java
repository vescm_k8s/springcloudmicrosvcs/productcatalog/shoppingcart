package org.vescm.shoppingcart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.vescm.shoppingcart.model.Cart;
import org.vescm.shoppingcart.model.Item;
import org.vescm.shoppingcart.repository.CartRepository;

import java.util.Optional;

@RestController
@RequestMapping(value = "/cart")
public class CartController {
    private final CartRepository cartRepository;

    @Autowired
    public CartController(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Cart addItem(@PathVariable("id") Integer id, @RequestBody Item item) {
        Optional<Cart> savedCart = cartRepository.findById(id);
        Cart cart = new Cart();
        if (savedCart.equals(Optional.empty())) {
            cart.setId(id);
        }
        else {
            if (savedCart.isPresent()) {
                cart = savedCart.get();
            }
        }
        cart.getItems().add(item);
        return cartRepository.save(cart);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Cart> findById(@PathVariable("id") Integer id) {
        return cartRepository.findById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void clear(@PathVariable("id") Integer id) {
        cartRepository.deleteById(id);
    }
}
