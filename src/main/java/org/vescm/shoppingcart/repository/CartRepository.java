package org.vescm.shoppingcart.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.vescm.shoppingcart.model.Cart;

@Repository
public interface CartRepository extends CrudRepository<Cart, Integer> {
    Cart findByCustomerId(Integer customerId);
}
